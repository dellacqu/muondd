get_filename_component(SELF_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(${SELF_DIR}/MuonDD-MuonAGDDDescription.cmake)
include(${SELF_DIR}/MuonDD-MuonGeoModel.cmake)
include(${SELF_DIR}/MuonDD-MuonAGDDBase.cmake)

